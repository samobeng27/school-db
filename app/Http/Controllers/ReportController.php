<?php

namespace App\Http\Controllers;

use App\Company;
use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ReportController extends Controller
{
    public function datatable()
    {

        return view('report.report');
    }

    public function getPosts()
    {
        $comp=Company::all();
//        foreach ($comp as $comm){
//            $check=  $comm->city->name;
//            dd($check);
//        }
        return DataTables::of(Company::query()->with(['categories', 'city']))
            ->addColumn('city_id', function(Company $com) {
            return $com->city->name;
        })
            ->addColumn('categories', function(Company $com) {
                return $com->categories()->pluck('name')->toArray();
//                return $com->categories()[0]->name;
        })
//            ->addColumn('gendert', function(Company $com) {
////                return $com->categories()->pluck('name')->toArray();
//                return $com->gender_type or '';
//        })


            ->make(true);

    }
}
