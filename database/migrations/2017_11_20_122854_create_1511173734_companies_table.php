<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1511173734CompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('companies')) {
            Schema::create('companies', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name')->nullable();
//                $table->string('city_id')->nullable();
                $table->string('categories')->nullable();
                $table->string('address')->nullable();
                $table->string('website')->nullable();
                $table->string('email')->nullable();
//                $table->string('address')->nullable();
                $table->string('contact')->nullable();
//                $table->string('contact')->nullable();
                $table->string('gender')->nullable();
                $table->string('gender_type')->nullable();
                $table->string('population')->nullable();
                $table->string('boys')->nullable();
                $table->string('girls')->nullable();
                $table->text('programme')->nullable();
                $table->string('head')->nullable();
                $table->string('contact_head')->nullable();
                $table->string('email_head')->nullable();
                $table->string('library_facility')->nullable();
                $table->string('reg_no')->nullable();
                $table->string('date_appointed_head')->nullable();
                $table->string('year_posted')->nullable();
                $table->date('head_dob')->nullable();
                $table->string('magazine')->nullable();
                $table->text('description')->nullable();
                $table->text('computer')->nullable();
                $table->text('books')->nullable();
                $table->text('library_librarian')->nullable();
                $table->text('librarian_train')->nullable();
                $table->string('logo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
