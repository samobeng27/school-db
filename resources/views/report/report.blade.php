@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.qa_report')</h3>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Report</div>

                <div class="panel-body">
                    <table class="table table-hover table-bordered table-striped datatable" width="100%">
                        <thead>
                        <tr>
{{--                            <th>Id</th>--}}
                            <th style="width:10% !important;">School</th>
                            <th style="width:10% !important;">Region</th>
                            <th style="width:10% !important;">District</th>
                            <th style="width:10% !important;">Address</th>
                            <th style="width:10% !important;">Contact</th>
                            <th style="width:10% !important;">website</th>
                            <th style="width:10% !important;">Email</th>
                            <th style="width:10% !important;">Registration Number</th>
                            <th style="width:10% !important;">Gender</th>
                            <th style="width:10% !important;">Gender Type</th>
                            <th style="width:10% !important;">Boys</th>
                            <th style="width:10% !important;">Girls</th>
                            <th style="width:10% !important;">Population</th>
                            <th style="width:10% !important;">Programme</th>
                            <th style="width:10% !important;">Head</th>
                            <th style="width:10% !important;">Contact Head</th>
                            <th style="width:10% !important;">Head's Email</th>
                            <th style="width:10% !important;">Date of Birth</th>
                            <th style="width:10% !important;">Date Appointed</th>
                            <th style="width:10% !important;">Year Posted</th>
                            <th style="width:10% !important;">Do you have a Library Facility?</th>
                            <th style="width:10% !important;">Do you have a Librarian?</th>
                            <th style="width:10% !important;">Is your Librarian Trained?</th>
                            <th style="width:10% !important;">Stock</th>
                            <th style="width:10% !important;">Are you Subscribed to any Magazine or Journal?</th>
                            <th style="width:10% !important;">Do you have a Computer Facility?</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.1.0/js/dataTables.buttons.min.js"></script>

{{--<script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>--}}
<script type="text/javascript">
    $(document).ready(function() {
        $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            "scrollY": 250,
            scrollX: true,
            fixedColumns: false,            // overflow:true,
            dom: 'Bfrtip',
            buttons: [
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                'print'
            ],
            // dom:'Bfrtip',
            ajax: '{{ route('admin.datatable/getdata') }}',

            columns: [
                // {data: 'id', name: 'id'},
                {data: 'name', name: 'name',orderable: true, searchable: true },
                {data: 'city.name', name: 'city.name',orderable: true, searchable: true},
                {data: 'categories', name: 'categories',orderable: true, searchable: true},
                {data: 'address', name: 'address',orderable: true, searchable: true},
                {data: 'contact', name: 'contact',orderable: true, searchable: true},
                {data: 'website', name: 'website',orderable: true, searchable: true},
                {data: 'email', name: 'email',orderable: true, searchable: true},
                {data: 'reg_no', name: 'reg_no',orderable: true, searchable: true},
                {data: 'gender', name: 'gender',orderable: true, searchable: true},
                {data: 'gender_type', name: 'gender_type',orderable: true, searchable: true},
                {data: 'boys', name: 'boys',orderable: true, searchable: true},
                {data: 'girls', name: 'girls',orderable: true, searchable: true},
                {data: 'population', name: 'population',orderable: true, searchable: true},
                {data: 'programme', name: 'programme',orderable: true, searchable: true},
                {data: 'head', name: 'head',orderable: true, searchable: true},
                {data: 'contact_head', name: 'contact_head',orderable: true, searchable: true},
                {data: 'email_head', name: 'email_head',orderable: true, searchable: true},
                {data: 'head_dob', name: 'head_dob',orderable: true, searchable: true},
                {data: 'date_appointed_head', name: 'date_appointed_head',orderable: true, searchable: true},
                {data: 'year_posted', name: 'year_posted',orderable: true, searchable: true},
                {data: 'library_facility', name: 'library_facility',orderable: true, searchable: true},
                {data: 'library_librarian', name: 'library_librarian',orderable: true, searchable: true},
                {data: 'librarian_train', name: 'librarian_train',orderable: true, searchable: true},
                {data: 'books', name: 'books',orderable: true, searchable: true},
                {data: 'magazine', name: 'magazine',orderable: true, searchable: true},
                {data: 'computer', name: 'computer',orderable: true, searchable: true},
            ]
        });
    });
</script>