@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.companies.title')</h3>
    
{{--    {!! Form::model($company, ['method' => 'PUT', 'route' => ['admin.companies.update', $company->id], 'files' => true,]) !!}--}}

{{--    <div class="panel panel-default">--}}
{{--        <div class="panel-heading">--}}
{{--            @lang('quickadmin.qa_edit')--}}
{{--        </div>--}}

{{--        <div class="panel-body">--}}
{{--            <div class="row">--}}
{{--                <div class="col-xs-12 form-group">--}}
{{--                    {!! Form::label('name', trans('quickadmin.companies.fields.name').'', ['class' => 'control-label']) !!}--}}
{{--                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '']) !!}--}}
{{--                    <p class="help-block"></p>--}}
{{--                    @if($errors->has('name'))--}}
{{--                        <p class="help-block">--}}
{{--                            {{ $errors->first('name') }}--}}
{{--                        </p>--}}
{{--                    @endif--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="row">--}}
{{--                <div class="col-xs-12 form-group">--}}
{{--                    {!! Form::label('city_id', trans('quickadmin.companies.fields.city').'', ['class' => 'control-label']) !!}--}}
{{--                    {!! Form::select('city_id', $cities, old('city_id'), ['class' => 'form-control select2']) !!}--}}
{{--                    <p class="help-block"></p>--}}
{{--                    @if($errors->has('city_id'))--}}
{{--                        <p class="help-block">--}}
{{--                            {{ $errors->first('city_id') }}--}}
{{--                        </p>--}}
{{--                    @endif--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="row">--}}
{{--                <div class="col-xs-12 form-group">--}}
{{--                    {!! Form::label('categories', trans('quickadmin.companies.fields.categories').'', ['class' => 'control-label']) !!}--}}
{{--                    {!! Form::select('categories[]', $categories, old('categories') ? old('categories') : $company->categories->pluck('id')->toArray(), ['class' => 'form-control select2', 'multiple' => 'multiple']) !!}--}}
{{--                    <p class="help-block"></p>--}}
{{--                    @if($errors->has('categories'))--}}
{{--                        <p class="help-block">--}}
{{--                            {{ $errors->first('categories') }}--}}
{{--                        </p>--}}
{{--                    @endif--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="row">--}}
{{--                <div class="col-xs-12 form-group">--}}
{{--                    {!! Form::label('address', trans('quickadmin.companies.fields.address').'', ['class' => 'control-label']) !!}--}}
{{--                    {!! Form::text('address', old('address'), ['class' => 'form-control', 'placeholder' => '']) !!}--}}
{{--                    <p class="help-block"></p>--}}
{{--                    @if($errors->has('address'))--}}
{{--                        <p class="help-block">--}}
{{--                            {{ $errors->first('address') }}--}}
{{--                        </p>--}}
{{--                    @endif--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="row">--}}
{{--                <div class="col-xs-12 form-group">--}}
{{--                    {!! Form::label('description', trans('quickadmin.companies.fields.description').'', ['class' => 'control-label']) !!}--}}
{{--                    {!! Form::textarea('description', old('description'), ['class' => 'form-control ', 'placeholder' => '']) !!}--}}
{{--                    <p class="help-block"></p>--}}
{{--                    @if($errors->has('description'))--}}
{{--                        <p class="help-block">--}}
{{--                            {{ $errors->first('description') }}--}}
{{--                        </p>--}}
{{--                    @endif--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="row">--}}
{{--                <div class="col-xs-12 form-group">--}}
{{--                    @if ($company->logo)--}}
{{--                        <a href="{{ asset(env('UPLOAD_PATH').'/'.$company->logo) }}" target="_blank"><img src="{{ asset(env('UPLOAD_PATH').'/thumb/'.$company->logo) }}"></a>--}}
{{--                    @endif--}}
{{--                    {!! Form::label('logo', trans('quickadmin.companies.fields.logo').'', ['class' => 'control-label']) !!}--}}
{{--                    {!! Form::hidden('logo', old('logo')) !!}--}}
{{--                    {!! Form::file('logo', ['class' => 'form-control', 'style' => 'margin-top: 4px;']) !!}--}}
{{--                    {!! Form::hidden('logo_max_size', 5) !!}--}}
{{--                    {!! Form::hidden('logo_max_width', 4096) !!}--}}
{{--                    {!! Form::hidden('logo_max_height', 4096) !!}--}}
{{--                    <p class="help-block"></p>--}}
{{--                    @if($errors->has('logo'))--}}
{{--                        <p class="help-block">--}}
{{--                            {{ $errors->first('logo') }}--}}
{{--                        </p>--}}
{{--                    @endif--}}
{{--                </div>--}}
{{--            </div>--}}

{{--        </div>--}}
{{--    </div>--}}

{{--    {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!}--}}
{{--    {!! Form::close() !!}--}}
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_edit')
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        {!! Form::model($company, ['method' => 'PUT', 'route' => ['admin.companies.update', $company->id], 'files' => true,]) !!}

        <div class="panel-body step">
            <div class="alert alert-info">
                <strong>Info!</strong> Provide Information of the School By filling these form
            </div>
            <div class="">
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('name', trans('quickadmin.companies.fields.name').'', ['class' => 'control-label']) !!}
                        {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('name'))
                            <p class="help-block">
                                {{ $errors->first('name') }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('city_id', trans('quickadmin.companies.fields.city').'', ['class' => 'control-label']) !!}
                        {!! Form::select('city_id', $cities, old('city_id'), ['class' => 'city form-control select2','id'=>'city']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('city_id'))
                            <p class="help-block">
                                {{ $errors->first('city_id') }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('categories', trans('quickadmin.companies.fields.categories').'', ['class' => 'control-label']) !!}
                        {!! Form::select('categories', $categories, old('categories') ? old('categories') : $company->categories->pluck('id')->toArray(), ['class' => 'form-control select2','id'=>'catme']) !!}

{{--                        <select name="categories" id="" class=" select form-control">--}}
{{--                            @foreach($categories as $category)--}}

{{--                            @endforeach--}}
{{--                        </select>--}}
                        <p class="help-block"></p>
                        @if($errors->has('categories'))
                            <p class="help-block">
                                {{ $errors->first('categories') }}
                            </p>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('website', trans('quickadmin.companies.fields.website').'', ['class' => 'control-label']) !!}
                        {!! Form::text('website', old('website'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('email'))
                            <p class="help-block">
                                {{ $errors->first('email') }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('email', trans('quickadmin.companies.fields.email').'', ['class' => 'control-label']) !!}
                        {!! Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('email'))
                            <p class="help-block">
                                {{ $errors->first('email') }}
                            </p>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('address', trans('quickadmin.companies.fields.address').'', ['class' => 'control-label']) !!}
                        {!! Form::text('address', old('address'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('address'))
                            <p class="help-block">
                                {{ $errors->first('address') }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('contact', trans('quickadmin.companies.fields.contact').'', ['class' => 'control-label']) !!}
                        {!! Form::text('contact', old('contact'), ['class' => 'form-control', 'placeholder' => '','id'=>'txtNumber','maxlength'=>'10','onkeypress'=>'return isNumberKey(event)' ]) !!}
                        <p class="help-block"></p>
                        @if($errors->has('contact'))
                            <p class="help-block">
                                {{ $errors->first('contact') }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('reg_no', trans('quickadmin.companies.fields.reg_no').'', ['class' => 'control-label']) !!}
                        {!! Form::text('reg_no', old('reg_no'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('reg_no'))
                            <p class="help-block">
                                {{ $errors->first('reg_no') }}
                            </p>
                        @endif
                    </div>
                </div>


            </div>


        </div>
        <div class="panel-body step">
            <div class="alert alert-info">
                <strong>Info!</strong> Provide the Population of the School and Programme offered  By filling these form
            </div>

            <div class="">
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('gender', trans('quickadmin.companies.fields.gender').'', ['class' => 'control-label']) !!}
                        <br>
                        <span style="margin-right: 3% !important;" >Single Sex&nbsp &nbsp{{ Form::radio('gender', 'single' , true,['onclick' => 'show1();','id'=>'single']) }}</span>
                        <span >Mixed  &nbsp &nbsp{{ Form::radio('gender', 'mixed' , false,['onclick' => 'show2();','id'=>'mixed']) }}</span>

                        <p class="help-block"></p>
                        @if($errors->has('gender'))
                            <p class="help-block">
                                {{ $errors->first('gender') }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="row single_sex" id="div2">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('population', trans('quickadmin.companies.fields.total_number').'', ['class' => 'control-label']) !!}
                        <br>
                        <span style="margin-right: 3% !important;">Boys &nbsp &nbsp{{ Form::radio('gender_type', 'boys' , false,['id'=>'bb','class'=>'gen_type']) }}</span>
                        <span >Girls  &nbsp &nbsp{{ Form::radio('gender_type', 'girls' , false,['id'=>'gg','class'=>'gen_type']) }}</span><br><br>

                        <p class="help-block"></p>
                        @if($errors->has('gender'))
                            <p class="help-block">
                                {{ $errors->first('gender') }}
                            </p>
                        @endif
                    </div>
                </div>

                <div class="row mixed_sex" id="div1" style="display: none">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('population', trans('quickadmin.companies.fields.total_number').'', ['class' => 'control-label']) !!}
                        <br>
                        <span style="margin-right: 3% !important;">Boys {!! Form::text('boys', old('boys'), ['class' => 'add form-control', 'placeholder' => '','id'=>'boys','maxlength'=>'200000000000000000000000','onkeypress'=>'return isNumberKey(event)' ]) !!}</span>
                        <span >Girls {!! Form::text('girls', old('girls'), ['class' => 'add form-control', 'placeholder' => '','id'=>'girls','maxlength'=>'200000000000000000000000','onkeypress'=>'return isNumberKey(event)' ]) !!}</span><br>

                        <p class="help-block"></p>
                        @if($errors->has('gender'))
                            <p class="help-block">
                                {{ $errors->first('gender') }}
                            </p>
                        @endif
                    </div>
                </div>
                {{--                        <div class="row">--}}
                {{--                            <div class="col-xs-12 form-group">--}}
                Total Number of Student
                {!! Form::text('population', old('population'), ['class' => 'population form-control ', 'id'=>'total','placeholder' => 'Number or Lists']) !!}
                <p class="help-block"></p>
                @if($errors->has('population'))
                    <p class="help-block">
                        {{ $errors->first('population') }}
                    </p>
                @endif
                {{--                            </div>--}}
                {{--                        </div>--}}
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('programme', trans('quickadmin.companies.fields.programme').'', ['class' => 'control-label']) !!}
                        {!! Form::textarea('programme', old('programme'), ['class' => 'form-control ', 'placeholder' => 'Number or Lists']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('programme'))
                            <p class="help-block">
                                {{ $errors->first('programme') }}
                            </p>
                        @endif
                    </div>
                </div>

            </div>
        </div>
        <div class="panel-body step">
            <div class="alert alert-info">
                <strong>Info!</strong> Head of School Details
            </div>
            <div class="row">

                <div class="col-xs-12 form-group">
                    {!! Form::label('head', trans('quickadmin.companies.fields.head').'', ['class' => 'control-label']) !!}
                    {!! Form::text('head', old('head'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('head'))
                        <p class="help-block">
                            {{ $errors->first('head') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="">

                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('contact_head', trans('quickadmin.companies.fields.contact_head').'', ['class' => 'control-label']) !!}
                        {!! Form::text('contact_head', old('contact_head'), ['class' => 'form-control', 'placeholder' => '','onkeypress'=>'return isNumberKey(event)']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('contact_head'))
                            <p class="help-block">
                                {{ $errors->first('contact_head') }}
                            </p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="">

                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('email_head', trans('quickadmin.companies.fields.email_head').'', ['class' => 'control-label']) !!}
                        {!! Form::text('email_head', old('email_head'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('email_head'))
                            <p class="help-block">
                                {{ $errors->first('email_head') }}
                            </p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="">

                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('head_dob', trans('quickadmin.companies.fields.head_dob').'', ['class' => 'control-label']) !!}
                        {!! Form::text('head_dob', old('head_dob'), ['class' => 'date form-control', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('head_dob'))
                            <p class="help-block">
                                {{ $errors->first('head_dob') }}
                            </p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="">

                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('date_appointed_head', trans('quickadmin.companies.fields.date_appointed_head').'', ['class' => 'control-label']) !!}
                        {!! Form::text('date_appointed_head', old('date_appointed_head'), ['class' => 'date form-control', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('date_appointed_head'))
                            <p class="help-block">
                                {{ $errors->first('date_appointed_head') }}
                            </p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="">

                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('year_posted', trans('quickadmin.companies.fields.year_posted').'', ['class' => 'control-label']) !!}
                        {!! Form::text('year_posted', old('year_posted'), ['class' => 'year form-control', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('year_posted'))
                            <p class="help-block">
                                {{ $errors->first('year_posted') }}
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body step">
            <div class="alert alert-info">
                <strong>Info!</strong> Library and I.C.T information
            </div>
            {{--            <div class="">--}}

            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('library_facility', trans('quickadmin.companies.fields.library_facility').'', ['class' => 'control-label']) !!}
                    <br>
                    <span style="margin-right: 3% !important;" >Yes &nbsp &nbsp{{ Form::radio('library_facility', 'yes' , false,['id' => 'yes']) }}</span>
                    <span >No  &nbsp &nbsp{{ Form::radio('library_facility', 'no' , true,['id' => 'no']) }}</span>

                    <p class="help-block"></p>
                    @if($errors->has('library_facility'))
                        <p class="help-block">
                            {{ $errors->first('library_facility') }}
                        </p>
                    @endif
                </div>
                {{--                </div>--}}
            </div>
            <div class="books" style="display: none">

                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('books', trans('quickadmin.companies.fields.books').'', ['class' => 'control-label']) !!}
                        {!! Form::text('books', old('books'), ['class' => 'form-control', 'placeholder' => '','onkeypress'=>'return isNumberKey(event)']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('books'))
                            <p class="help-block">
                                {{ $errors->first('books') }}
                            </p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="books" style="display: none">

                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('library_librarian', trans('quickadmin.companies.fields.library_librarian').'', ['class' => 'control-label']) !!}
                        <br>
                        <span style="margin-right: 3% !important;" >Yes &nbsp &nbsp{{ Form::radio('library_librarian', 'yes') }}</span>
                        <span >No  &nbsp &nbsp{{ Form::radio('library_librarian', 'no' ) }}</span>

                        <p class="help-block"></p>
                        @if($errors->has('library_librarian'))
                            <p class="help-block">
                                {{ $errors->first('library_librarian') }}
                            </p>
                        @endif
                    </div>
                </div>
            </div>
            {{--            <div class="books">--}}

            {{--                <div class="row">--}}
            {{--                    <div class="col-xs-12 form-group">--}}
            {{--                        {!! Form::label('librarian_tutor', trans('quickadmin.companies.fields.librarian_tutor').'', ['class' => 'control-label']) !!}--}}
            {{--                        <br>--}}
            {{--                        <span style="margin-right: 3% !important;" >Yes &nbsp &nbsp{{ Form::radio('librarian_tutor', 'yes' , true) }}</span>--}}
            {{--                        <span >No  &nbsp &nbsp{{ Form::radio('librarian_tutor', 'no' , false,) }}</span>--}}

            {{--                        <p class="help-block"></p>--}}
            {{--                        @if($errors->has('librarian_tutor'))--}}
            {{--                            <p class="help-block">--}}
            {{--                                {{ $errors->first('librarian_tutor') }}--}}
            {{--                            </p>--}}
            {{--                        @endif--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--                </div>--}}
            <div class="books" style="display: none">

                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('librarian_train', trans('quickadmin.companies.fields.librarian_train').'', ['class' => 'control-label']) !!}
                        <br>
                        <span style="margin-right: 3% !important;" >Yes &nbsp &nbsp{{ Form::radio('librarian_train', 'yes') }}</span>
                        <span >No  &nbsp &nbsp{{ Form::radio('librarian_train', 'no') }}</span>

                        <p class="help-block"></p>
                        @if($errors->has('librarian_train'))
                            <p class="help-block">
                                {{ $errors->first('librarian_train') }}
                            </p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="">

                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('magazine', trans('quickadmin.companies.fields.magazine').'', ['class' => 'control-label']) !!}
                        <br>
                        <span style="margin-right: 3% !important;" >Yes &nbsp &nbsp{{ Form::radio('magazine', 'yes' , false) }}</span>
                        <span >No  &nbsp &nbsp{{ Form::radio('magazine', 'no' , true) }}</span>

                        <p class="help-block"></p>
                        @if($errors->has('magazine'))
                            <p class="help-block">
                                {{ $errors->first('magazine') }}
                            </p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="">

                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('computer', trans('quickadmin.companies.fields.computer').'', ['class' => 'control-label']) !!}
                        <br>
                        <span style="margin-right: 3% !important;" >Yes &nbsp &nbsp{{ Form::radio('computer', 'yes' , false) }}</span>
                        <span >No  &nbsp &nbsp{{ Form::radio('computer', 'no' , true) }}</span>

                        <p class="help-block"></p>
                        @if($errors->has('computer'))
                            <p class="help-block">
                                {{ $errors->first('computer') }}
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        {{--                <div class="step ">Step 5</div>--}}
        {{--                <div class="step ">Step 6</div>--}}
        <div class="panel-body step ">
            <div class="alert alert-info">
                <strong>Info!</strong> A short Description of School and Logo/badge
            </div>
            <div class="">
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('description', trans('quickadmin.companies.fields.description').'', ['class' => 'control-label']) !!}
                        {!! Form::textarea('description', old('description'), ['class' => 'form-control ', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('description'))
                            <p class="help-block">
                                {{ $errors->first('description') }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('logo', trans('quickadmin.companies.fields.logo').'', ['class' => 'control-label']) !!}
                        {!! Form::hidden('logo', old('logo')) !!}
                        {!! Form::file('logo', ['class' => 'form-control', 'style' => 'margin-top: 4px;']) !!}
                        {!! Form::hidden('logo_max_size', 5) !!}
                        {!! Form::hidden('logo_max_width', 4096) !!}
                        {!! Form::hidden('logo_max_height', 4096) !!}
                        <p class="help-block"></p>
                        @if($errors->has('logo'))
                            <p class="help-block">
                                {{ $errors->first('logo') }}
                            </p>
                        @endif
                    </div>
                </div>                    </div>
            {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!}
        </div>
        <a class="btn btn-default action back">Back</a>
        <a class="btn btn-primary action next">Next</a>
        {{--                <button class="action submit btn btn-success">Submit</button>--}}

        @include('ajaxwork.ajaxcall')

        {!! Form::close() !!}
    </div>

@stop

