@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.companies.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body ">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.companies.fields.name')</th>
                            <td field-key='name'>{{ $company->name }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.companies.fields.city')</th>
                            <td field-key='city'>{{ $company->city->name or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.companies.fields.categories')</th>
                            <td field-key='categories'>
                                @foreach ($company->categories as $singleCategories)
                                    <span class="label label-info label-many">{{ $singleCategories->name }}</span>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.companies.fields.address')</th>
                            <td field-key='address'>{{ $company->address }}</td>
                        </tr>

                        <tr>
                            <th>@lang('quickadmin.companies.fields.contact')</th>
                            <td field-key='address'>{{ $company->contact }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.companies.fields.website')</th>
                            <td field-key='address'>{{ $company->website }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.companies.fields.email')</th>
                            <td field-key='address'>{{ $company->email }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.companies.fields.reg_no')</th>
                            <td field-key='address'>{{ $company->reg_no }}</td>
                        </tr>
                        @if($company->gender==='single')
                        <tr>
                            <th>@lang('quickadmin.companies.fields.gender')</th>
                            <td field-key='address'>{{ $company->gender .' '.' '.'Sex' }}</td>
                        </tr>
                            <tr>
                            <th>@lang('quickadmin.gender_type')</th>
                            <td field-key='address'>{{ $company->gender_type }}</td>
                        </tr>
                        @elseif($company->gender==='mixed')
                            <tr>
                                <th>@lang('quickadmin.companies.fields.gender')</th>
                                <td field-key='address'>{{ $company->gender .' '.' '.'Sex' }}</td>
                            </tr>
                            <tr>
                                <th>Boys</th>
                                <td field-key='address'>{{ $company->boys}}</td>
                            </tr>
                            <tr>
                                <th>Girls</th>
                                <td field-key='address'>{{ $company->girls}}</td>
                            </tr>
                        @endif
                        <tr>
                            <th>Total Number of Student</th>
                            <td field-key='description'>{!! $company->population !!}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.companies.fields.programme')</th>
                            <td field-key='description'>{!! $company->programme !!}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.companies.fields.head')</th>
                            <td field-key='description'>{!! $company->head !!}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.companies.fields.contact_head')</th>
                            <td field-key='description'>{!! $company->contact_head !!}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.companies.fields.email_head')</th>
                            <td field-key='description'>{!! $company->email_head !!}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.companies.fields.date_appointed_head')</th>
                            <td field-key='description'>{{ date_format(date_create($company->date_appointed_head),'d M Y') }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.companies.fields.year_posted')</th>
                            <td field-key='description'>{!! $company->year_posted !!}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.companies.fields.head_dob')</th>
                            <td field-key='description'>{{ date_format(date_create($company->head_dob),'d M Y') }}</td>
                        </tr>
                        @if($company->library_facility ==='yes')
                        <tr>
                            <th>@lang('quickadmin.companies.fields.library_facility')</th>
                            <td field-key='description'>{!! $company->library_facility !!}</td>
                        </tr>
                            <tr>
                            <th>@lang('quickadmin.companies.fields.books')</th>
                            <td field-key='description'>{!! $company->books !!}</td>
                        </tr>
                            <tr>
                            <th>@lang('quickadmin.companies.fields.library_librarian')</th>
                            <td field-key='description'>{!! $company->library_librarian !!}</td>
                        </tr>
                            <tr>
                            <th>@lang('quickadmin.companies.fields.librarian_train')</th>
                            <td field-key='description'>{!! $company->librarian_train !!}</td>
                        </tr>
                        @elseif($company->library_facility ==='no')
                            <tr>
                                <th>@lang('quickadmin.companies.fields.library_facility')</th>
                                <td field-key='description'>{!! $company->library_facility !!}</td>
                            </tr>


                        @endif
                        <tr>
                            <th>@lang('quickadmin.companies.fields.magazine')</th>
                            <td field-key='description'>{!! $company->magazine !!}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.companies.fields.computer')</th>
                            <td field-key='description'>{!! $company->computer !!}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.companies.fields.description')</th>
                            <td field-key='description'>{!! $company->description !!}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.companies.fields.logo')</th>
                            <td field-key='logo'>@if($company->logo)<a href="{{ asset(env('UPLOAD_PATH').'/' . $company->logo) }}" target="_blank"><img src="{{ asset(env('UPLOAD_PATH').'/thumb/' . $company->logo) }}"/></a>@endif</td>
                        </tr>
                    </table>
                </div>
            </div>


            <p>&nbsp;</p>

            <a href="{{ route('admin.companies.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
