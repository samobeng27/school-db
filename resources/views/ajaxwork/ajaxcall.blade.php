<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
   
<script type="text/javascript">

    $(document).ready(function() {

        $('select[name="city_id"]').on('change', function() {

            var cityID = $(this).val();
            if(cityID) {

                $.ajax({

                    url: '/myform/ajax/'+cityID,

                    type: "GET",

                    dataType: "json",

                    success:function(data) {


                        

                        $('select[name="categories"]').empty();

                        $.each(data, function(key, value) {

                            $('select[name="categories"]').append('<option value="'+ key +'">'+ value +'</option>');

                        });


                    }

                });

            }else{

                $('select[name="categories"]').empty();

            }

        });

    });


</script>
<script>
    $(document).ready(function(){
        var current = 1;

        widget      = $(".step");
        btnnext     = $(".next");
        btnback     = $(".back");
        btnsubmit   = $(".submit");

        // Init buttons and UI
        widget.not(':eq(0)').hide();
        hideButtons(current);
        setProgress(current);

        // Next button click action
        btnnext.click(function(){
            if(current < widget.length) {
                widget.show();
                widget.not(':eq('+(current++)+')').hide();
                setProgress(current);
                //alert("I was called from btnNext");
            }
            hideButtons(current);
        });

        // Back button click action
        btnback.click(function(){
            if(current > 1){
                current = current - 2;
                btnnext.trigger('click');
            }
            hideButtons(current);
        });
    });

    // Change progress bar action
    setProgress = function(currstep){
        var percent = parseFloat(100 / widget.length) * currstep;
        percent = percent.toFixed();
        $(".progress-bar")
            .css("width",percent+"%")
            .html(percent+"%");
    }

    // Hide buttons according to the current step
    hideButtons = function(current){
        var limit = parseInt(widget.length);

        $(".action").hide();

        if(current < limit) btnnext.show();
        if(current > 1) btnback.show();
        if(current == limit) { btnnext.hide(); btnsubmit.show(); }
    }
</script>

<script>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }


    function isNumericKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return true;
        return false;
    }
</script>
<script>
    function show1(){
        document.getElementById('div1').style.display ='none';
        document.getElementById('div2').style.display = 'block';
    }
    function show2(){
        document.getElementById('div1').style.display = 'block';
        document.getElementById('div2').style.display = 'none';
    }
</script>
<script>
    $(function() {
        $("input[name='library_facility']").click(function() {
            if ($("#yes").is(":checked")) {
                $(".books").show();
            } else {
                $(".books").hide();
            }
        });
    });
</script>
<script>

    $('#girls').keyup(function(){
        var boys;
        var girls;
        boys = parseFloat($('#boys').val());
        girls = parseFloat($('#girls').val());
        var result = boys + girls;
        $('#total').val(result);
    });


</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
{{--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">--}}
{{--<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css" />--}}
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script>
    $('.date').datepicker({
        format: 'mm-dd-yyyy'
    });
</script>
<script>
    // *** (year only) ***
    $(".year").datepicker({
        // changeMonth: false,
        changeYear: true,
        dateFormat: 'yy',
        duration: 'fast',
        // stepMonths: 0
    });
</script>
<script>
    $(document).ready(function() {
        // $('#catme').attr('disabled', true);
        // $("#catme").attr("disabled", "disabled");
        $('#catme option:not(:selected)').attr('disabled', true);
    });
    $(document).ready(function(){
        $("#city").change(function(){
            $('#catme').attr('disabled', false);
        });
    });
</script>
<script>
    $(function() {
        $('input[type="radio"]').click(function() {
            if($(this).attr('id') == 'single') {
                $('#boys').val('');
                $('#girls').val('');
                $('#total').val('');
            } else if($(this).attr('id') == 'mixed') {
                // $('#others-text').hide();
                // $('.gen_type').val('');
                $('input[name=gender_type]').prop('checked', false);
                $('#total').val('');
            }else if($(this).attr('id') == 'no') {
                $('#books').val('');
                $('input[name=library_librarian]').prop('checked', false);
                $('input[name=librarian_train]').prop('checked', false);



            }
        });
    });
    // $("#mixed").trigger('click');
    $(document).ready(function(){
        if ($('#mixed').attr("checked") == "checked") {
            $("#mixed").trigger('click');
            var boys;
            var girls;
            boys = parseFloat($('#boys').val());
            girls = parseFloat($('#girls').val());
            var result = boys + girls;
            $('.population').val(result);
        }

    });
    $(document).ready(function(){
        if ($('#yes').attr("checked") == "checked") {
            $("#yes").trigger('click');

        }

    });
</script>